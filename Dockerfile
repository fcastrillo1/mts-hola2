FROM websphere-liberty:latest

RUN chown -R 1001:0 /config/dropins/
COPY target/demo-openshift-0.0.1-SNAPSHOT.war /config/dropins/